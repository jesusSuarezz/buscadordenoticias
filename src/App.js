import React,{Fragment, useState, useEffect} from 'react'
import Header from './Components/Header'
import Form from './Components/Form';
import ListadoNoticias from './Components/ListadoNoticias';

function App() {

  // Definir la categoria y noticias
  const [categoria, guardarCategoria] = useState('')
  const [noticias, guardarNoticias]= useState([])

  useEffect(()=>{
		const consultarAPI = async () => {
			const url = `https://newsapi.org/v2/top-headlines?country=mx&category=${categoria}&apiKey=e5b2033025de4fccb42850d00df2b893`;

      console.log(url);
			const response = await fetch(url);
			const news = await response.json();
			guardarNoticias(news.articles);

			console.log(noticias);
		};

		consultarAPI();

		
	}, [categoria])

  return (
    <Fragment>
      <Header titulo='Buscador Noticias de México (Jesús Suárez)'/>

      <div className="container white">
        <Form
          guardarCategoria={guardarCategoria}
        />

        <ListadoNoticias
          noticias={noticias}
        />
      </div>
    </Fragment>
  );
}

export default App;
