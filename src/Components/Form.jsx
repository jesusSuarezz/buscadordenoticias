import React from 'react';
import PropTypes from 'prop-types';

import styles from './Form.module.css';
import useSelect from '../hooks/useSelect';


const Form = ({guardarCategoria }) => {
	const OPCIONES = [
		{ value: 'general', label: 'General' },
		{ value: 'technology', label: 'Tecnología' },
		{ value: 'business', label: 'Negocios' },
		{ value: 'science', label: 'Ciencia' },
		{ value: 'health', label: 'Salud' },
		{ value: 'entertainment', label: 'Entretenimiento' },
	];

	// Utilizar nuestro custom hook
    const [categoria, SelectNoticias] = useSelect('general', OPCIONES);
    
    // Submit al form, pasar a app.js
    const buscarNoticias = (e)=>{
        e.preventDefault()
        //Pasamos la categoria del select al state del app.js
        guardarCategoria(categoria)
    }

	return (
		<div className={`${styles.buscador} row`}>
			<div className="col s12 m8 offset-m2">
				<form onSubmit={buscarNoticias}>
					<h2 className={styles.heading}>Encuentra Noticias por categoria</h2>

					<SelectNoticias />

					<div className="input-field col s12">
						<button
							className={`${styles['btn-block']} btn-large waves-effect light-blue darken-3`}
							type="submit"
						>
							Buscar
							<i className="material-icons right">search</i>
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

Form.propTypes = { 
	guardarCategoria: PropTypes.func.isRequired,
};

export default Form;
